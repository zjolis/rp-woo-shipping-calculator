<div id="rp_shipping_calculator">
    <div class="rp_shipping_button"><a href="javascript:void(0);" class="btn_shipping"><?php echo ($this->get_setting('button_text') != "") ? $this->get_setting('button_text') : __("Calculate Shipping", "rpship"); ?></a></div>
    <div class="rp_shiiping_form">
        <?php if ($this->get_setting("display_message") != 1): ?>
            <div class="rp_message"></div>
        <?php endif; ?>

        <form class="woocommerce-shipping-calculator" action="" method="post" onkeydown="return event.key != 'Enter';">
            <section class="shipping-calculator-form">
                <?php
                if (is_product()) {
                    global $post;
                    ?>
                    <input type="hidden" name="product_id" value="<?php echo $post->ID; ?>" />
                <?php } ?>
                <p class="form-row form-row-wide" style="display: none;">
                    <select name="calc_shipping_country" id="calc_shipping_country" class="country_to_state" rel="calc_shipping_state">
                        <option value=""><?php  _e('Select a country', 'woocommerce'); ?></option>
                        <?php
                         foreach (WC()->countries->get_shipping_countries() as $key => $value)
                             echo '<option value="' . esc_attr($key) . '"' . selected(WC()->customer->get_shipping_country(), esc_attr($key), false) . '>' . esc_html($value) . '</option>';
                        ?>
                    </select>
                </p>

                <p class="form-row form-row-wide shipping_state" style="display: none;">
                    <?php
                    $current_cc = WC()->customer->get_shipping_country();
                    $current_r = WC()->customer->get_shipping_state();
                    $states = WC()->countries->get_states($current_cc);
                    if (is_array($states) && empty($states)) {
                        ?>
                        <input type="hidden" name="calc_shipping_state" class="text-state" id="calc_shipping_state" placeholder="<?php _e('State / county', 'woocommerce'); ?>" />
                        <?php
                    } elseif (is_array($states)) {
                        ?>

                        <select name="calc_shipping_state" id="calc_shipping_state" placeholder="<?php _e('State / county', 'woocommerce'); ?>">
                            <option value=""><?php _e('Select a state&hellip;', 'woocommerce'); ?></option>
                            <?php
                            foreach ($states as $ckey => $cvalue)
                                echo '<option value="' . esc_attr($ckey) . '" ' . selected($current_r, $ckey, false) . '>' . __(esc_html($cvalue), 'woocommerce') . '</option>';
                            ?>
                        </select>

                        <?php
                    } else {
                        ?>
                        <input type="text" class="input-text" value="<?php  echo esc_attr($current_r); ?>" placeholder="<?php  _e('State / county', 'woocommerce'); ?>" name="calc_shipping_state" id="calc_shipping_state" />
                        <?php
                    }
                    ?>
                </p>

                <?php if (apply_filters('woocommerce_shipping_calculator_enable_postcode', true)) : ?>

                    <p class="form-row form-row-wide shipping_postcode">
                        <label for="calc_shipping_postcode"><?php echo _e("Enter Delivery postcode to find soonest delivery date") ?></label>
                        <span>
                            <input type="text" class="input-text" placeholder="<?php _e('Postcode / Zip', 'woocommerce'); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
                            <button class="button" type="button" id="calculate_delivery_date">Find Shipping Dates</button>
                        </span>
                    </p>

                <?php endif; ?>
                <span class="loaderimage"><img src="<?php echo self::$plugin_url ?>assets/images/rp-loader.gif" alt=""></span>
                <div class="shippingmethod_container">
                    <?php
                    $packages = WC()->cart->get_shipping_packages();
                    $packages = WC()->shipping->calculate_shipping($packages);
                    $available_methods = WC()->shipping->get_packages();
                    if ($this->get_setting("button_pos") == 3) {
                        // $this->generateOrderDeliveryDatesByShippingMethods($available_methods[0]["rates"]);
                    } else if ($this->get_setting('shipping_type') == 1) {
                        if (isset($available_methods[0]["rates"]) && count($available_methods[0]["rates"]) > 0) {
                            foreach ($available_methods[0]["rates"] as $key => $method) {
                                echo '<input name="calc_shipping_method" class="shipping_method" type="radio" ' . checked($key, WC()->session->chosen_shipping_method, false) . ' value="' . esc_attr($key) . '">&nbsp;' . wp_kses_post($method->label) . "<br>";
                            }
                        }
                    } else {
                        ?>
                        <select name="calc_shipping_method" id="calc_shipping_method" class="shipping_method">
                            <option value=""><?php _e('Select a Shipping Method ', 'woocommerce'); ?></option>
                            <?php
                            if (isset($available_methods[0]["rates"]) && count($available_methods[0]["rates"]) > 0) {
                                foreach ($available_methods[0]["rates"] as $key => $method) {
                                    echo '<option value="' . esc_attr($key) . '" ' . selected($key, WC()->session->chosen_shipping_method, false) . '>' . wp_kses_post($method->label) . '</option>';
                                }
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                </div>    
                <?php if ($this->get_setting("shipping_type") != 2 && $this->get_setting("button_pos") != 3): ?>
                <p>
                    <button type="submit"  name="rp_calc_shipping" value="1" class="rp_calc_shipping button"><?php _e('Calculate', 'rpship'); ?></button>
                    <span class="loaderimage"><img src="<?php echo self::$plugin_url ?>assets/images/rp-loader.gif" alt=""></span>
                </p>
                <?php endif; ?>
                <strong>You can choose your preferred shipping option on the CART page</strong>
                <?php if ($this->get_setting("display_message") == 1): ?>
                    <div class="rp_message"></div>
                <?php endif; ?>
                <?php wp_nonce_field('woocommerce-cart'); ?>
                <a href="#" class="close-link">Close</a>
            </section>
        </form>
    </div>

</div>
