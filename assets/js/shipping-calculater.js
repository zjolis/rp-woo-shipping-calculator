(function ($) {
    $(document).ready(function () {
        $("#calc_shipping_postcode").on('keydown', function(event){
          console.log('pressed');
          
          if( (event.keyCode == 13) ) {
            event.preventDefault();
            $(this).trigger('blur');
            return false;
          }
        });
        $('.woocommerce-shipping-calculator').on('submit', function(e) {
          e.preventDefault();
          $("#calc_shipping_postcode").trigger('blur');
        })
        $(document).on("change","#calc_shipping_method",function(){
            $('.rp_calc_shipping').trigger('click');
        });
        $(".btn_shipping").click(function () {
            $(".rp_shiiping_form").toggle("slow");
        });
        $(".single_variation_wrap").on("show_variation", function (event, variation) {
            $(".loaderimage").show();
            element=$('.country_to_state,.shipping_state select');
            var datastring = element.closest(".woocommerce-shipping-calculator").serialize();
            if($("input.variation_id").length>0){
                datastring=datastring+"&variation_id="+$("input.variation_id").val();
            }
            if($("input[name=quantity]").length>0){
                datastring=datastring+"&current_qty="+$("input[name=quantity]").val();
            }
            $.ajax({
                type: "POST",
                url: rp_ajax_url+"?action=update_shipping_method",
                data: datastring,
                success: function (data) {
                    $(".loaderimage").hide();
                    element.parent().parent().find('.shippingmethod_container').html(data);
                }
            });
        });

        $('.rp_calc_shipping').click(function () {
            $(".loaderimage").show();
            var datastring = $(this).closest(".woocommerce-shipping-calculator").serialize();
            if($("input.variation_id").length>0){
                datastring=datastring+"&variation_id="+$("input.variation_id").val();
            }
            if($("input[name=quantity]").length>0){
                datastring=datastring+"&current_qty="+$("input[name=quantity]").val();
            }
            $.ajax({
                type: "POST",
                url: rp_ajax_url+"?action=ajax_calc_shipping",
                data: datastring,
                dataType: 'json',
                success: function (data) {
                    $(".loaderimage").hide();
                    $(".rp_message").removeClass("rp_error").removeClass("rp_success");
                    if (data.code == "error") {
                        $(".rp_message").html(data.message).addClass("rp_error");
                    } else if (data.code == "success") {
                        $(".rp_message").html(data.message).addClass("rp_success");
                    } else {
                        return true;
                    }
                }
            });
            return false;
        });
        
        $('.country_to_state,.shipping_state select').change(function () {
            $(".loaderimage").show();
            element=$(this);
            var datastring = $(this).closest(".woocommerce-shipping-calculator").serialize();
            if($("input.variation_id").length>0){
                datastring=datastring+"&variation_id="+$("input.variation_id").val();
            }
            if($("input[name=quantity]").length>0){
                datastring=datastring+"&current_qty="+$("input[name=quantity]").val();
            }
            $.ajax({
                type: "POST",
                url: rp_ajax_url+"?action=update_shipping_method",
                data: datastring,
                success: function (data) {
                    $(".loaderimage").hide();
                    element.parent().parent().find('.shippingmethod_container').html(data);
                }
            });
            
        });
        $('.shipping_postcode input,.shipping_state input').blur(function () {
            $(".loaderimage").show();
            element=$(this);
            var datastring = $(this).closest(".woocommerce-shipping-calculator").serialize();
            if($("input.variation_id").length>0){
                datastring=datastring+"&variation_id="+$("input.variation_id").val();
            }
            if($("input[name=quantity]").length>0){
                datastring=datastring+"&current_qty="+$("input[name=quantity]").val();
            }
            $.ajax({
                type: "POST",
                url: rp_ajax_url+"?action=update_shipping_method",
                data: datastring,
                success: function (data) {
                    $(".loaderimage").hide();
                    element.parent().parent().find('.shippingmethod_container').html(data);
                }
            });
            return false;
        });
    });
    jQuery("#calculate_delivery_date").on('click', function (e) {      
      fetchShippingMethods( jQuery(this) );
    });
    jQuery("#rp_shipping_calculator .close-link").on('click', function (e) {      
      fetchShippingMethods( jQuery(this) );
    });

    function fetchShippingMethods (element) {
        element.closest('.shipping-calculator-form').find('.shippingmethod_container').html("");
        jQuery(".loaderimage").show();

        datastring = "calc_shipping_postcode="+jQuery("input[name=calc_shipping_postcode]").val()+"&product_id="+jQuery("input[name=product_id]").val()+"&calc_shipping_state="+jQuery("select[name=calc_shipping_state]").val()+"&calc_shipping_country="+jQuery("select[name=calc_shipping_country]").val();

        if(jQuery("input.variation_id").length>0){
            datastring=datastring+"&variation_id="+jQuery("input.variation_id").val();
        }
        
        if(jQuery("input[name=quantity]").length>0){
            datastring=datastring+"&current_qty="+jQuery("input[name=quantity]").val();
        }
        
        jQuery.ajax({
            type: "POST",
            url: rp_ajax_url+"?action=update_shipping_method",
            data: datastring,
            success: function (data) {
                element.closest('.shipping-calculator-form').find('.shippingmethod_container').html(data);
                jQuery(".loaderimage").hide();
                setTimeout(function () {
                    element.closest('.orddd_pr_modal').removeClass("open");
                }, 5000);
            }
        });
        return false;
    }
})(jQuery);